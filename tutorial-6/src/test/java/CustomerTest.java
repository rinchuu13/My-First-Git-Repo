import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    Customer customer;
    Movie movie;
    Movie movie2;
    Rental rent;

    @Before
    public void setUp(){
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        movie = new Movie("Your Name", Movie.NEW_RELEASE);
        rent = new Rental(movie, 5);
        customer = new Customer("Taki");
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 15.0"));
        assertTrue(result.contains("2 frequent renter points"));

        movie = new Movie("A Silent Voice", Movie.CHILDREN);
        rent = new Rental(movie, 7);
        customer = new Customer("Ishida");
        customer.addRental(rent);

        result = customer.statement();
        lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 7.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementTest(){
        movie = new Movie("Fireworks", Movie.CHILDREN);
        rent = new Rental(movie, 7);
        customer = new Customer("Norimichi");
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4,lines.length);
        assertTrue(result.contains("<P>You owe <EM>7.5</EM><P>"));
        assertTrue(result.contains("you earned <EM>1</EM> frequent renter points<P>"));
    }
}
