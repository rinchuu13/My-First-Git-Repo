package tutorial.javari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class JavariDatabase {
    private List<Animal> animal;
    private final String path = "javari_data.csv";
    private final Path file = Paths.get("", path);

    public JavariDatabase() throws IOException {
        animal = new ArrayList<>();
        loadData();
    }

    public List<Animal> getAnimal() {
        return animal;
    }

    private void loadData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file.toString()));
        String line = reader.readLine();
        while (line != null) {

            animal.add(csvToAnimal(line));
            line = reader.readLine();
        }
        reader.close();
    }

    private void saveData() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file.toString()));
        for (Animal hewan : animal) {
            String csv = animalToCsv(hewan);
            writer.write(csv);
            writer.newLine();
        }
        writer.close();
    }

    public Animal addAnimal(String json) throws IOException, JSONException {
        Animal newAnimal = jsonToAnimal(json);
        if (!duplicateId(newAnimal)) {
            animal.add(newAnimal);
            saveData();
            return newAnimal;
        }
        return null;
    }

    private Animal jsonToAnimal(String input) throws JSONException {
        JSONObject json = new JSONObject(input);
        return new Animal(json.getInt("id"), json.getString("type"),
                json.getString("name"),
                Gender.parseGender(json.getString("gender")),
                json.getDouble("length"), json.getDouble("weight"),
                Condition.parseCondition(json.getString("condition")));
    }


    private boolean duplicateId(Animal subject) {
        for (Animal hewan : animal) {
            if (hewan.getId() == subject.getId()) {
                return true;
            }
        }
        return false;
    }

    public Animal getAnimalById(int id) {
        for (Animal hewan : animal) {
            if (hewan.getId() == id) {
                return hewan;
            }
        }
        return null;
    }

    public Animal deleteAnimalById(int id) throws IOException {
        Animal hewan = null;
        for (int i = 0; i < animal.size(); i++) {
            if (animal.get(i).getId() == id) {
                hewan = animal.remove(i);
                break;
            }
        }
        saveData();
        return hewan;
    }

    private Animal csvToAnimal(String csvInput) {
        String[] attrs = csvInput.split(",");
        return new Animal(Integer.parseInt(attrs[0]),
                attrs[1], attrs[2], Gender.parseGender(attrs[3]),
                Double.parseDouble(attrs[4]), Double.parseDouble(attrs[5]),
                Condition.parseCondition(attrs[6]));
    }

    private String animalToCsv(Animal animal) {
        String[] attrs = {animal.getId().toString(), animal.getType(),
                animal.getName(), animal.getGender().toString(),
                String.valueOf(animal.getLength()),
                String.valueOf(animal.getWeight()),
                animal.getCondition().toString()};
        return String.join(",", attrs);
    }
}
