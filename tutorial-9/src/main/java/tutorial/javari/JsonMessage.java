package tutorial.javari;

public class JsonMessage {
    private final String msgType;
    private final String message;

    private JsonMessage(String msgType, String message) {
        this.msgType = msgType;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getMsgType() {
        return msgType;
    }

    public static JsonMessage getNotFoundMessage(int id) {
        return new JsonMessage("warning",
                "Sorry, but it seems we have no animal with id no." + id);
    }

    public static JsonMessage getDatabaseEmptyMessage() {
        return new JsonMessage("warning",
                "Sorry, but it seems there's no animal in our database...");
    }

    public static JsonMessage getSuccessDeleteMessage() {
        return new JsonMessage("success",
                "We have successfully deleted the animal from our database. "
                        + "Here's the data in case you want to create it again some time");
    }

    public static JsonMessage getSuccessAddMessage() {
        return new JsonMessage("success",
                "We have successfully added the animal into our database. "
                        + "Here's the registered data of the animal");
    }

}
