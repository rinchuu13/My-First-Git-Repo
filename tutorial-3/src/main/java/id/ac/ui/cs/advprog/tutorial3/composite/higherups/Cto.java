package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    private String name;
    private double salary;
    private String role;
    private static final double minimumSalary = 100000.00;

    public Cto(String name, double salary) {
        this.name = name;
        if (salary < minimumSalary) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
