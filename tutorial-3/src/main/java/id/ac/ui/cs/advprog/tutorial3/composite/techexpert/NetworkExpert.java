package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    private String name;
    private double salary;
    private String rle;
    private static final double minimumSalary = 50000.00;

    public NetworkExpert(String name, double salary) {
        this.name = name;
        if (salary < minimumSalary) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
