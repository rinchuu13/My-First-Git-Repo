package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    private String name;
    private double salary;
    private String role;
    private static final double minimumSalary = 70000.00;

    public SecurityExpert(String name, double salary) {
        this.name = name;
        if (salary < minimumSalary) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
