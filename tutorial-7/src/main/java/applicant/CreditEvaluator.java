package applicant;

import java.util.function.Predicate;

public class CreditEvaluator implements Evaluator {

    public Predicate<Applicant> getEvaluator() {
        return applicant -> applicant.getCreditScore() > 600;
    }
}
