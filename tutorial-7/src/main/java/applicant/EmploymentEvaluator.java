package applicant;

import java.util.function.Predicate;

public class EmploymentEvaluator implements Evaluator {

    public Predicate<Applicant> getEvaluator() {
        return applicant -> applicant.getEmploymentYears() > 0;
    }
}
