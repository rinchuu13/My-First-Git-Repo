package applicant;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

import static applicant.Applicant.evaluate;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    Applicant applicant;
    Predicate<Applicant> qualifiedEvaluator;
    Predicate<Applicant> creditEvaluator;
    Predicate<Applicant> criminalEvaluator;
    Predicate<Applicant> employmentEvaluator;

    @Before
    public void setUp() {
        applicant = new Applicant();
        qualifiedEvaluator = new QualifiedEvaluator().getEvaluator();
        creditEvaluator = new CreditEvaluator().getEvaluator();
        criminalEvaluator = new CriminalRecordsEvaluator().getEvaluator();
        employmentEvaluator = new EmploymentEvaluator().getEvaluator();
    }

    @Test
    public void checkQualifiedEvaluator() {
        assertTrue(evaluate(applicant, qualifiedEvaluator));
    }

    @Test
    public void checkEmploymentEvaluator() {
        assertTrue(evaluate(applicant, employmentEvaluator));
    }

    @Test
    public void checkCreditEvaluator() {
        assertTrue(evaluate(applicant, creditEvaluator));
    }

    @Test
    public void checkCriminalEvaluator() {
        assertFalse(evaluate(applicant, criminalEvaluator));
    }

    @Test
    public void checkChainedEvaluator() {
        assertFalse(evaluate(applicant, qualifiedEvaluator
                .and(criminalEvaluator)));
    }
}
