import org.junit.Before;
import org.junit.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static junit.framework.TestCase.assertTrue;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    Map<String, Integer> score = new HashMap<>();
    Map<Integer, List<String>> group = new HashMap<>();

    @Before
    public void setUp(){
    	score.put("Uno",1);
    	score.put("Song",2);
    	score.put("Sarasa",3);
    	score.put("Quatre",4);
    	score.put("Funf",5);
    	score.put("Six",6);
    	score.put("Siete",7);
    	score.put("Octo",8);
    	score.put("Nio",9);
    	score.put("Esser",10);

    	group = ScoreGrouping.groupByScores(score);
    }
    @Test
    public void testGroup(){
    	assertTrue(group.get(1).contains("Uno"));
    	assertTrue(group.get(2).contains("Song"));
    	assertTrue(group.get(3).contains("Sarasa"));
    	assertTrue(group.get(4).contains("Quatre"));
    	assertTrue(group.get(5).contains("Funf"));
    	assertTrue(group.get(6).contains("Six"));
    	assertTrue(group.get(7).contains("Siete"));
    	assertTrue(group.get(8).contains("Octo"));
    	assertTrue(group.get(9).contains("Nio"));
    	assertTrue(group.get(10).contains("Esser"));
    } 
}