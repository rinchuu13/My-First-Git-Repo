package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PestoSauce;
import org.junit.Before;
import org.junit.Test;

public class PestoSauceTest {
    private PestoSauce pest;

    @Before
    public void setUp() {
        pest = new PestoSauce();
    }

    @Test
    public void tesToString() {
        assertEquals("Pesto sauce with tomatoes", pest.toString());
    }
}
