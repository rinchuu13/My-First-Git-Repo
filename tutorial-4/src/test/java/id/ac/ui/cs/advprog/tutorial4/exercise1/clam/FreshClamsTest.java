package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import org.junit.Before;
import org.junit.Test;

public class FreshClamsTest {
    private FreshClams fres;

    @Before
    public void setUp() {
        fresh = new FreshClams();
    }

    @Test
    public void tesToString() {
        assertEquals("Fresh Clams from Long Island Sound", fresh.toString());
    }
}
