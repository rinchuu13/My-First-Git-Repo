package id.ac.ui.cs.advprog.tutorial4.exercise1.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {

    private MozzarellaCheese moz;

    @Before
    public void setUp() {
        moz = new MozzarellaCheese();
    }

    @Test
    public void tesToString() {
        assertEquals("Shredded Mozzarella", moz.toString());
    }
}
