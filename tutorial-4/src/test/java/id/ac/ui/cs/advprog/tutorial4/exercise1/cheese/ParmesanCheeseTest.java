package id.ac.ui.cs.advprog.tutorial4.exercise1.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import org.junit.Before;
import org.junit.Test;

public class ParmesanCheeseTest {

    private ParmesanCheese par;

    @Before
    public void setUp() {
        par = new ParmesanCheese();
    }

    @Test
    public void tesToString() {
        assertEquals("Shredded Parmesan", par.toString());
    }
}
