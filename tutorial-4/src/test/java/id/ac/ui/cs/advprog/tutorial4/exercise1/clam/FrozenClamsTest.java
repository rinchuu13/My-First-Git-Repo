package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams froz;

    @Before
    public void setUp() {
        froz = new FrozenClams();
    }

    @Test
    public void tesToString() {
        assertEquals("Frozen Clams from Chesapeake Bay", froz.toString());
    }
}
