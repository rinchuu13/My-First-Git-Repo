package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import org.junit.Before;
import org.junit.Test;

public class EggplantTest {
    private Eggplant eggp;

    @Before
    public void setUp() {
        eggp = new Eggplant();
    }

    @Test
    public void tesToString() {
        assertEquals("Eggplant", eggp.toString());
    }
}
