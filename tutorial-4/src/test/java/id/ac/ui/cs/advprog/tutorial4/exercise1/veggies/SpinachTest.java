package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Before;
import org.junit.Test;

public class SpinachTest {
    private Spinach spin;

    @Before
    public void setUp() {
        spin = new Spinach();
    }

    @Test
    public void tesToString() {
        assertEquals("Spinach", spin.toString());
    }
}
