package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {
    private ThinCrustDough thin;

    @Before
    public void setUp() {
        thin = new ThinCrustDough();
    }

    @Test
    public void tesToString() {
        assertEquals("Thin Crust Dough", thin.toString());
    }
}
