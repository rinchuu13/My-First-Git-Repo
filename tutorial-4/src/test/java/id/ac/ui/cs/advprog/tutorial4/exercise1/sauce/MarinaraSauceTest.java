package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce mari;

    @Before
    public void setUp() {
        mari = new MarinaraSauce();
    }

    @Test
    public void tesToString() {
        assertEquals("Marinara Sauce", mari.toString());
    }
}
