package id.ac.ui.cs.advprog.tutorial4.exercise1.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {

    private ReggianoCheese reg;

    @Before
    public void setUp() {
        reg = new ReggianoCheese();
    }

    @Test
    public void tesToString() {
        assertEquals("Reggiano Cheese", reg.toString());
    }
}
