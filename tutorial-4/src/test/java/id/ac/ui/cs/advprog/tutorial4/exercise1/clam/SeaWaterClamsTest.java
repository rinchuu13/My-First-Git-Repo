package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SeaWaterClams;
import org.junit.Before;
import org.junit.Test;

public class SeaWaterClamsTest {
    private SeaWaterClams fres;

    @Before
    public void setUp() {
        seaw = new SeaWaterClams();
    }

    @Test
    public void tesToString() {
        assertEquals("Sea Water Clams from North Sea", seaw.toString());
    }
}
