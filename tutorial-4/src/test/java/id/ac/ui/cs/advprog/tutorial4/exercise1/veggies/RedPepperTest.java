package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import org.junit.Before;
import org.junit.Test;

public class RedPepperTest {
    private RedPepper redp;

    @Before
    public void setUp() {
        redp = new RedPepper();
    }

    @Test
    public void tesToString() {
        assertEquals("Red Pepper", redp.toString());
    }
}
