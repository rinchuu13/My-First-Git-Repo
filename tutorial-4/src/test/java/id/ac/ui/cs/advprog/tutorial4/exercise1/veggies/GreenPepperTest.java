package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.GreenPepper;
import org.junit.Before;
import org.junit.Test;

public class GreenPepperTest {
    private GreenPepper greenp;

    @Before
    public void setUp() {
        greenp = new GreenPepper();
    }

    @Test
    public void tesToString() {
        assertEquals("Green Pepper", greenp.toString());
    }
}
