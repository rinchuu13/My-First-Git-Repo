package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import org.junit.Before;
import org.junit.Test;

public class GarlicTest {
    private Garlic garl;

    @Before
    public void setUp() {
        garl = new Garlic();
    }

    @Test
    public void tesToString() {
        assertEquals("Garlic", garl.toString());
    }
}
