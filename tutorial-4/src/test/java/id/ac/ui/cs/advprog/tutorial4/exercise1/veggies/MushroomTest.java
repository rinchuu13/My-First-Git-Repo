package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import org.junit.Before;
import org.junit.Test;

public class MushroomTest {
    private Mushroom mush;

    @Before
    public void setUp() {
        mush = new Mushroom();
    }

    @Test
    public void tesToString() {
        assertEquals("Mushrooms", mush.toString());
    }
}
