package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    PizzaStore newYork;
    Pizza cheesePizza;
    Pizza clamPizza;
    Pizza veggiesPizza;

    @Before
    public void setUp() {
        newYork = new NewYorkPizzaStore();
        cheesePizza = newYork.orderPizza("cheese");
        clamPizza = newYork.orderPizza("clam");
        veggiesPizza = newYork.orderPizza("veggie");
    }

    @Test
    public void checkPizzaNames() {
        assertEquals("New York Style Cheese Pizza", cheesePizza.getName());
        assertEquals("New York Style Clam Pizza", clamPizza.getName());
        assertEquals("New York Style Veggie Pizza", veggiesPizza.getName());
    }

    @Test
    public void checkPizzaDescriptions() {
        assertNotNull(clamPizza.toString());
        assertNotNull(veggiesPizza.toString());
        assertNotNull(cheesePizza.toString());
    }
}
