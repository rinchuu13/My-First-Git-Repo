package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CrustDough;
import org.junit.Before;
import org.junit.Test;

public class CrustDoughTest {
    private CrustDough cru;

    @Before
    public void setUp() {
        cru = new CrustDough();
    }

    @Test
    public void tesToString() {
        assertEquals("Crust style with crust dough", cru.toString());
    }
}
