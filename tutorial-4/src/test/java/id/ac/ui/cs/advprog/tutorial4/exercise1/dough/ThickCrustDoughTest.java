package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private ThickCrustDough thicc;

    @Before
    public void setUp() {
        thicc = new ThickCrustDough();
    }

    @Test
    public void tesToString() {
        assertEquals("ThickCrust style extra thick crust dough", thicc.toString());
    }
}
