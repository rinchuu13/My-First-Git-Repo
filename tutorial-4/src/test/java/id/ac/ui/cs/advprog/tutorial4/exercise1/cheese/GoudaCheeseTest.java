package id.ac.ui.cs.advprog.tutorial4.exercise1.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese;
import org.junit.Before;
import org.junit.Test;

public class GoudaCheeseTest {

    private GoudaCheese gou;

    @Before
    public void setUp() {
        gou = new GoudaCheese();
    }

    @Test
    public void tesToString() {
        assertEquals("Shredded Gouda Cheese", gou.toString());
    }
}
