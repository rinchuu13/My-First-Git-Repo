package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SeaWaterClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PestoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlive;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.GreenPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class NewYorkPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new CrustDough();
    }

    public Sauce createSauce() {
        return new PestoSauce();
    }

    public Cheese createCheese() {
        return new GoudaCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlive(), new Eggplant(), new Spinach(), new GreenPepper()};
        return veggies;
    }

    public Clams createClam() {
        return new SeaWaterClams();
    }
}
