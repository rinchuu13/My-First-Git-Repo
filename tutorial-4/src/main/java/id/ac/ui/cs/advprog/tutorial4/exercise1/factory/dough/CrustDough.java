package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CrustDough implements Dough {
    public String toString() {
        return "Crust style with crust dough";
    }
}
